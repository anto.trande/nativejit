NativeJIT
====

NativeJIT is an open-source cross-platform library for high-performance
just-in-time compilation of expressions involving C data structures.
The compiler is light weight and fast
and it takes no dependencies beyond the standard C++ runtime.
It runs on Linux, OSX, and Windows.
The generated code is optimized with particular attention paid
to register allocation.

The compiler was developed by the [Bing](http://www.bing.com/) team for use in the Bing search engine.
One important use is scoring documents containing keywords that match a user's query.
The scoring process attempts to gauge how well each document matches the user's intent,
and as such, depends on the specifics of how each query was phrased.
Bing formulates a custom expression for each query
and then uses NativeJIT to compile the expression into x64 code that will
be run on a large set of candidate documents spread across a cluster of
machines.

We knew from the get go that throughput and latency
would be essential when processing queries at scale,
so we put a lot of effort in to making NativeJIT run fast.

Our design point was scenarios where

* The expression isn't known until runtime.
* The expression will be evaluated enough times to amortize the cost of compilation.
* Latency and throughput demands require low cost for compilation.


Here's trivial "Hello, world" level example that computes the area of a circle:

```cpp
#include "NativeJIT/CodeGen/ExecutionBuffer.h"
#include "NativeJIT/CodeGen/FunctionBuffer.h"
#include "NativeJIT/Function.h"
#include "Temporary/Allocator.h"

#include <iostream>

using NativeJIT::Allocator;
using NativeJIT::ExecutionBuffer;
using NativeJIT::Function;
using NativeJIT::FunctionBuffer;

int main()
{
    // Create allocator and buffers for pre-compiled and post-compiled code.
    ExecutionBuffer codeAllocator(8192);
    Allocator allocator(8192);
    FunctionBuffer code(codeAllocator, 8192);

    // Create the factory for expression nodes.
    // Our area expression will take a single float parameter and return a float.
    Function<float, float> expression(allocator, code);

    // Multiply input parameter by itself to get radius squared.
    auto & rsquared = expression.Mul(expression.GetP1(), expression.GetP1());

    // Multiply by PI.
    const float  PI = 3.14159265358979f;
    auto & area = expression.Mul(rsquared, expression.Immediate(PI));

    // Compile expression into a function.
    auto function = expression.Compile(area);

    // Now run our expression!
    float radius = 2.0;
    std::cout << "The area of a circle with radius " << radius
              << " is " << function(radius);

    return 0;
}
```

Here is the generated assembly code on Windows:

```asm
PI_CONSTANT:
   db 0f 49 40                              ; PI constant is stored in memory.
ENTRY_POINT:
  sub         rsp,8                         ; Standard function prologue.
  mov         qword ptr [rsp],rbp           ; Standard function prologue.
  lea         rbp,[rsp+8]                   ; Standard function prologue.
  mulss       xmm0,xmm0                     ; Multiply by radius parameter by itself.
  mulss       xmm0,dword ptr [29E2A580000h] ; Multiply by PI.
  mov         rbp,qword ptr [rsp]           ; Standard function epilogue.
  add         rsp,8                         ; Standard function epilogue.
```


This example shows an expression that multiplies a number by itself.
We also support a wide variety of arithmetic and logical operations, pointer and array operations, conditionals, accessing structure fields, and calling out to C functions.
[See our preliminary API docs for more information](http://bitfunnel.org/getting-started-with-nativejit/) and the `Examples/` directory for more examples.


Dependencies
------------

In order to build NativeJIT you will need CMake (3.12+), google-test, and a modern C++
compiler (gcc 5+, clang 3.4+). You can run CMake directly to generate the appropriate build setup for your platform. Alternately, we have some scripts that have the defaults that we use available.

### *nix

For *nix platforms

```sh
./Configure_Make.sh
cd build-make
make
make test
```

#### Fedora

